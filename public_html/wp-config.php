<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_outmemphis_org_wordpress');

/** MySQL database username */
define('DB_USER', 'test_outmemphis_org');

/** MySQL database password */
define('DB_PASSWORD', 'WYV9r2g2AONCpMt');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'nlanfpmDhIg7eVlkjlVD57qvEdDGipFdY1Q1LB7ELCBJzdEq8gFpDS601ewKQdOa');
define('SECURE_AUTH_KEY', 'ntA3owXb7sdGerfUsC5aVgQIJFgU26iHJuTViRkO09YLBrjw0WCdLtSqnt0TytOP');
define('LOGGED_IN_KEY', '4XRG6cjDMHMAzqB9MMuMMZbPHjFnElYJikK8CIUlmSiBaBSG8V0cYHelcGv9wOAy');
define('NONCE_KEY', 'TMxbJUY3kYYc1HyYQQPAwcEZwpgHmPgnX0IYRB7m8131R9a3FKWCqSdYTGC26Ug1');
define('AUTH_SALT', 'WGMno0prvVZsWiVvXHuMzXEijxecUYE2LqccUb1qRjOerbdgKtfFwvTXJZ2Wldsl');
define('SECURE_AUTH_SALT', 'zy4FhvDv57VfLqbfYrdRFs9i0r7Th3sfQk6LrrNDbVIsUz4I49BKekkSXQRdp8LW');
define('LOGGED_IN_SALT', 'hWpMgp4Mn7M2HEQmD10ZDhnA48DzIr0yS0zIZZaN0FVBYdz3HC4WzFX7krSugowB');
define('NONCE_SALT', 'RRyVI7KYXPFIKrXpR8dFzXSOHqBi78UOQfSwvFjYSrBmuNFY7rUUkaaROwjdAKMM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
